function Pokemon(name, lvl, hp){
	this.name = name;
	this.level = lvl;
	this.health = hp * 2;
	this.attack = lvl;
	this.tackle =  function(target){
		//console.log(target)	//contains charizard obj

		console.log(`${this.name} tackled ${target.name}`);

		let currentHp = target.health = target.health - this.attack;
		console.log(`${target.name}'s health is now reduced to ${currentHp}`);

		
		if(currentHp < 10){

			this.faint(target.name);
		}
	};
	this.faint = function(target){
		console.log(`${target} fainted`);
	}

}

let meowt = new Pokemon("Meowt", 10, 50);
let balbasaur = new Pokemon("Balbasaur", 8, 46);
// let squirtle = new Pokemon("squirtle", 4, 80);

console.log(meowt.tackle(balbasaur))
console.log(meowt.tackle(balbasaur))
console.log(meowt.tackle(balbasaur))
console.log(meowt.tackle(balbasaur))
console.log(meowt.tackle(balbasaur))
console.log(meowt.tackle(balbasaur))
console.log(meowt.tackle(balbasaur))
console.log(meowt.tackle(balbasaur))
console.log(meowt.tackle(balbasaur))


//faint function invoke when health below 10